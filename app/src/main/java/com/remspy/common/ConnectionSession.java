package com.remspy.common;

import com.remspy.interfaces.OnPackageConfirm;

import java.util.ArrayList;

public class ConnectionSession {

    private DuplicateSparseArray<OnPackageConfirm> confirmCallbacksArray = new DuplicateSparseArray<>();

    private int mPackageId = (int) (System.currentTimeMillis() / 1000);

    public void newPackage() {
        mPackageId++;
    }

    public int getPackageId() {
        return mPackageId;
    }

    public void onPackageConfirmSubscribe(OnPackageConfirm cb) {
        onPackageConfirmSubscribe(mPackageId, cb);
    }

    public void onPackageConfirmSubscribe(int packageId, OnPackageConfirm cb) {
        confirmCallbacksArray.put(packageId, cb);
    }

    public boolean onPackageConfirm(int packageId) {
        ArrayList<OnPackageConfirm> confirmCallbacks = confirmCallbacksArray.get(packageId);
        if (confirmCallbacks == null) {
            return false;
        }

        for (OnPackageConfirm cb: confirmCallbacks) {
            cb.onPackageConfirm(packageId);
        }

        return true;
    }

}
