package com.remspy.common;

import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.provider.BaseColumns;
import android.util.Log;

import com.remspy.annotations.Inject;
import com.remspy.interfaces.ModulePermission;
import com.remspy.interfaces.ModuleStart;
import com.remspy.modules.holders.ContextHolder;
import com.remspy.modules.holders.RealmHolder;
import com.remspy.scheme.ProviderRecord;
import com.remspy.services.AppService;
import com.remspy.utils.ArrayUtils;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

public abstract class ContentProvider extends ContentObserver implements ModuleStart, ModulePermission {

    @Inject
    protected RealmHolder mRealmHolder;

    @Inject
    protected ContextHolder mContextHolder;

    @Inject
    private AppService mAppService;

    private SortedIntMap mProviderRecordsCache;

    public ContentProvider(Handler handler) {
        super(handler);
    }

    @Override
    public void onChange(boolean selfChange) {
        this.onChange(selfChange, null);
    }

    private ScheduleLock schedulerLock;
    
    protected String[] getProjectionColumns() {
        return new String[] { BaseColumns._ID };
    }
    
    protected int getRowVersion(Cursor row) {
        return 0;
    }

    private SortedIntMap getResolverRecords() throws Exception {
        Cursor res = mContextHolder.getContentResolver().query(getUri(), getProjectionColumns(), null, null, BaseColumns._ID + " ASC");
        if (res == null) {
            Log.d("Remspy", "Query url: " + getUri());
            throw new Exception("Resolver query return null");
        }

        SortedIntMap resolverRecords = new SortedIntMap(res.getCount());

        if (res.moveToFirst()) {
            int index = res.getColumnIndex(BaseColumns._ID);

            do {
                resolverRecords.put(res.getInt(index), getRowVersion(res));
            } while (res.moveToNext());
        }

        res.close();
        return resolverRecords;
    }

    private SortedIntMap getProviderRecords(Realm realm) throws Exception {
        if (mProviderRecordsCache != null) {
            return mProviderRecordsCache;
        }

        Uri uri = getUri();

        RealmResults<ProviderRecord> providerRecords = realm.where(ProviderRecord.class)
                .equalTo("provider", uri.hashCode())
                .findAllSorted("record", Sort.ASCENDING);

        mProviderRecordsCache = new SortedIntMap(providerRecords.size());

        for (ProviderRecord record: providerRecords) {
            mProviderRecordsCache.put(record.getRecord(), record.getVersion());
        }

        return mProviderRecordsCache;
    }

    private Runnable onChangeRunnable = new Runnable() {
        @Override
        public void run() {
            Realm realm = mRealmHolder.db();
            if (realm == null) {
                return;
            }

            try {
                SortedIntMap providerRecords = getProviderRecords(realm);
                SortedIntMap resolverRecords = getResolverRecords();

                int[] newRecords = providerRecords.getNotExistKeys(resolverRecords.getKeys());
                int[] removedRecords = resolverRecords.getNotExistKeys(providerRecords.getKeys());
                int[] updatedRecords = providerRecords.diffValues(resolverRecords);

                if (newRecords.length > 0 || removedRecords.length > 0 || updatedRecords.length > 0) {
                    realm.beginTransaction();

                    onChange(realm, newRecords, removedRecords, updatedRecords);
                    int provider = getUri().hashCode();

                    for (int id: newRecords) {
                        int version = resolverRecords.get(id);

                        realm.createObject(ProviderRecord.class)
                            .setProvider(provider)
                            .setRecord(id)
                            .setVersion(version);

                        mProviderRecordsCache.put(id, version);
                    }

                    if (removedRecords.length > 0) {
                        realm.where(ProviderRecord.class)
                                .equalTo("provider", provider)
                                .in("record", ArrayUtils.asIntegerArray(removedRecords))
                                .findAll()
                                .deleteAllFromRealm();

                        mProviderRecordsCache.deleteAll(removedRecords);
                    }

                    if (updatedRecords.length > 0) {
                        RealmResults<ProviderRecord> updatedProviderRecords = realm.where(ProviderRecord.class)
                                .equalTo("provider", provider)
                                .in("record", ArrayUtils.asIntegerArray(updatedRecords))
                                .findAll();

                        for (ProviderRecord record: updatedProviderRecords) {
                            int id = record.getRecord();
                            int newVersion = resolverRecords.get(id);

                            providerRecords.update(id, newVersion);
                            record.setVersion(newVersion);
                        }
                    }

                    realm.commitTransaction();
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                realm.close();
            }
        }
    };

    @Override
    public void onChange(final boolean selfChange, final Uri uri) {
        Log.d("Remspy", "onChange: " + uri);

        schedulerLock.lock(600, onChangeRunnable);
    }

    @Override
    public void onStart() {
        schedulerLock = new ScheduleLock(mAppService.getHandler());

        onChange(false, null);
    }

    public abstract void onChange(Realm realm, int[] newRecords, int[] removedRecords, int[] updatedRecords);
    public abstract Uri getUri();

}
