package com.remspy.common;

import android.util.SparseArray;

import java.util.ArrayList;

public class DuplicateSparseArray<V> {

    private SparseArray<ArrayList<V>> m = new SparseArray<>();

    public void put(int k, V v) {
        if (m.indexOfKey(k) > 0) {
            m.get(k).add(v);
        } else {
            ArrayList<V> arr = new ArrayList<>();
            arr.add(v);
            m.put(k, arr);
        }
    }

    public ArrayList<V> get(int k) {
        return m.get(k);
    }

    public void remove(int k) {
        m.remove(k);
    }

}