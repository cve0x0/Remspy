package com.remspy.common;

import android.app.Notification;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.support.v7.app.NotificationCompat;

import com.remspy.constants.NotificationConstants;

public abstract class IntentService extends Service {
    private volatile Looper mServiceLooper;
    private volatile Handler mHandler;
    private String mName;

    public IntentService(String name) {
        super();
        mName = name;
    }

    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {

        }

        public void onServiceDisconnected(ComponentName className) {

        }
    };

    private void startForegroundNotification() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            return;
        }

        Notification notification = new NotificationCompat.Builder(getApplicationContext())
                .build();

        startForeground(NotificationConstants.NOTIFICATION_ID, notification);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        HandlerThread thread = new HandlerThread("IntentService[" + mName + "]");
        thread.start();

        mServiceLooper = thread.getLooper();
        mHandler = new Handler(mServiceLooper) {
            @Override
            public void handleMessage(Message message) {
                onMessage(message);
            }
        };
    }

    final public Handler getHandler() {
        return mHandler;
    }

    public void onMessage(Message message) {

    }

    @Override
    public int onStartCommand(@Nullable final Intent intent, int flags, int startId) {
        if (intent != null) {
            startForegroundNotification();
            getApplicationContext().bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        }

        mHandler.post(new Runnable() {
            @Override
            public void run() {
                onHandleIntent(intent);
            }
        });

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        mServiceLooper.quit();
    }

    @Override
    @Nullable
    public IBinder onBind(Intent intent) {
        return null;
    }

    @WorkerThread
    protected abstract void onHandleIntent(@Nullable Intent intent);
}

