package com.remspy.common;

import com.remspy.constants.ProtocolConstants;
import com.remspy.net.PackageReader;
import com.remspy.net.utils.BufferUtils;

import java.nio.ByteBuffer;

public class PackageBufferReader extends PackageReader {

    public PackageBufferReader() {
        super();
    }

    public PackageBufferReader(boolean havePackageId) {
        super(havePackageId);
    }

    @Override
    protected ByteBuffer initHeaderBuffer(int size) {
        return BufferUtils.allocate(++size);
    }

    @Override
    protected boolean onParseHeader() {
        return mPackageHeader.get() == ProtocolConstants.BUFFER_START;
    }

}
