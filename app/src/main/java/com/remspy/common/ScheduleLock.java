package com.remspy.common;

import android.os.Handler;

@SuppressWarnings("WeakerAccess")
public class ScheduleLock {

    private Handler mHandler;
    private long mLastMills, mMills;
    private boolean mIsDelayStarted = false;
    private Runnable mCallbackRunnable;

    public ScheduleLock(Handler handler) {
        mHandler = handler;
    }

    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            long i = System.currentTimeMillis() - mLastMills;

            if (i < mMills) {
                mHandler.postDelayed(this, i);
                return;
            }

            mCallbackRunnable.run();
            mIsDelayStarted = false;
        }
    };

    public void lock(long mills, Runnable callbackRunnable) {
        mMills = mills;
        mCallbackRunnable = callbackRunnable;
        mLastMills = System.currentTimeMillis();

        if (!mIsDelayStarted) {
            mHandler.postDelayed(mRunnable, mMills);
            mIsDelayStarted = true;
        }
    }

}