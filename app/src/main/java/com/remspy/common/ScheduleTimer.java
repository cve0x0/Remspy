package com.remspy.common;

import android.os.Handler;
import android.util.Log;

@SuppressWarnings("WeakerAccess")
public abstract class ScheduleTimer implements Runnable {

    private int mInterval;
    private boolean mStatus = false;
    private Handler mHandler;

    protected abstract void onTick();

    @Override
    public void run() {
        onTick();

        if (isWorking()) {
            try {
                mHandler.postDelayed(this, mInterval);
            } catch (Exception ex) {
                Log.d("Remspy", ex.getMessage());
            }
        }
    }

    public ScheduleTimer setHandler(Handler handler) {
        mHandler = handler;
        return this;
    }

    public int getInterval() {
        return mInterval;
    }

    public ScheduleTimer setInterval(int interval) {
        mInterval = interval;
        return this;
    }

    public boolean isWorking() {
        return mStatus;
    }

    public void start() {
        mStatus = true;
        run();
    }

    public void stop() {
        mStatus = false;
    }

}
