package com.remspy.common;

import com.remspy.utils.ArrayUtils;

@SuppressWarnings({"WeakerAccess", "FieldCanBeLocal"})
public class SortedIntMap {

    private int mSize, mPos;
    private int[] mKeys;
    private int[] mValues;

    public SortedIntMap() {
        this(10);
    }

    public SortedIntMap(int size) {
        mKeys = new int[size];
        mValues = new int[size];
        mSize = size;
    }

    public int get(int key) {
        return get(key, 0);
    }

    public int get(int key, int valueIfKeyNotFound) {
        int i = ArrayUtils.binarySearch(mKeys, mPos, key);

        if (i < 0) {
            return valueIfKeyNotFound;
        } else {
            return mValues[i];
        }
    }

    public void deleteAll(int[] keys) {
        for (int key: keys) {
            delete(key);
        }
    }

    public void delete(int key) {
        int i = ArrayUtils.binarySearch(mKeys, mPos, key);

        if (i >= 0) {
            removeAt(i);
        }
    }

    public void removeAt(int index) {
        System.arraycopy(mKeys, index + 1, mKeys, index, mPos - (index + 1));
        System.arraycopy(mValues, index + 1, mValues, index, mPos - (index + 1));
        mSize--;
        mPos--;
    }

    public void put(int key, int value) {
        if (mPos >= mSize) {
            mSize++;
            mKeys = ArrayUtils.resize(mKeys, mSize);
            mValues = ArrayUtils.resize(mValues, mSize);
        }

        mKeys[mPos] = key;
        mValues[mPos] = value;
        mPos++;
    }

    public void update(int key, int value) {
        int i = ArrayUtils.binarySearch(mKeys, mPos, key);

        if (i >= 0) {
            mValues[i] = value;
        }
    }

    public int size() {
        return mSize;
    }

    public int keyAt(int index) {
        return mKeys[index];
    }

    public int valueAt(int index) {
        return mValues[index];
    }

    public void setValueAt(int index, int value) {
        mValues[index] = value;
    }

    public int indexOfKey(int key) {
        return ArrayUtils.binarySearch(mKeys, mSize, key);
    }

    public int indexOfValue(int value) {
        for (int i = 0; i < mSize; i++)
            if (mValues[i] == value)
                return i;
        return -1;
    }

    public void clear() {
        mSize = 0;
    }

    public int[] getNotExistKeys(int[] arr) {
        return getNotExistKeys(arr, -1);
    }

    public int[] getNotExistKeys(int[] arr, int expected) {
        if (expected == -1) {
            if (mPos < arr.length - 1) {
                expected = arr.length - mPos;
            } else {
                expected = 0;
            }
        }

        int pos = 0;
        int[] res = new int[expected];

        for (int key: arr) {
            if (get(key, -1) != -1) {
                continue;
            }

            if (pos > res.length - 1) {
                res = ArrayUtils.resize(res, pos + 1);
            }

            res[pos++] = key;
        }

        return ArrayUtils.resize(res, pos);
    }

    public int[] diffValues(SortedIntMap map) {
        int[] res = new int[0];
        int pos = 0;

        for (int i = 0; i < mPos; ++i) {
            int key = mKeys[i];
            int value = mValues[i];

            int sValue = map.get(key);
            if (sValue != 0 && sValue != value) {
                if (pos > res.length - 1) {
                    res = ArrayUtils.resize(res, pos + 1);
                }

                res[pos++] = key;
            }
        }

        return res;
    }

    public int[] getKeys() {
        return ArrayUtils.resize(mKeys, mPos);
    }

    public int[] getValues() {
        return ArrayUtils.resize(mValues, mPos);
    }

}