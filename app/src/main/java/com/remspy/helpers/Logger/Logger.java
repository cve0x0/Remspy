package com.remspy.helpers.Logger;

public class Logger {

    private static boolean DEBUG_ON;
    private static LEVEL DEBUG_LEVEL;

    public static enum LEVEL { LOW, MEDIUM, HIGH }

    public Logger(boolean DEBUG_ON, LEVEL DEBUG_LEVEL) {
        Logger.DEBUG_ON = DEBUG_ON;
        Logger.DEBUG_LEVEL = DEBUG_LEVEL;
    }

    public static void d(Thread sender, Object msg) {
        StackTraceElement[] stack = sender.getStackTrace();

        for (StackTraceElement element : stack) {
            if (!element.isNativeMethod()) {
                String className = element.getClassName();
                String fileName = element.getFileName();
                int lineNumber = element.getLineNumber();
                String methodName = element.getMethodName();
            }
        }

//        Log.d()
    }

}
