package com.remspy.interfaces;

public interface ModulePermission {

    String[] permissions();

}
