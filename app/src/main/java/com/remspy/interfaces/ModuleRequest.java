package com.remspy.interfaces;

import com.remspy.common.ConnectionSession;
import com.remspy.net.BufferStream;

import io.realm.Realm;

public interface ModuleRequest {

    void onRequest(Realm realm, BufferStream buff, ConnectionSession session) throws Exception;

}
