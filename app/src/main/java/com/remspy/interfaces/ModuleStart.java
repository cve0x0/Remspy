package com.remspy.interfaces;

public interface ModuleStart {

    void onStart();

}
