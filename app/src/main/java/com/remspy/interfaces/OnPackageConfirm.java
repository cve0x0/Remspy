package com.remspy.interfaces;

public interface OnPackageConfirm {

    void onPackageConfirm(int packageId);

}
