package com.remspy.modules.providers.browser.models;

import com.remspy.scheme.IResolverRecord;

import io.realm.RealmObject;

public class WebsiteModel extends RealmObject implements IResolverRecord {

    private int id;

    private String url;

    @Override
    public int getId() {
        return id;
    }

}
