package com.remspy.modules.providers.contacts.interfaces;

public interface ContactsFieldsItr {

    String getFieldType(String label, int typeId);

}
