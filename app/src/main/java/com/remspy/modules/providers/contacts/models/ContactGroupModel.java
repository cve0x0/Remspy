package com.remspy.modules.providers.contacts.models;

import com.remspy.net.annotations.TransferField;
import com.remspy.net.enums.TransferFieldTypes;

import io.realm.RealmObject;

public class ContactGroupModel extends RealmObject {

    @TransferField(type = TransferFieldTypes.ChunkString, order = 0)
    private String data;

    @TransferField(type = TransferFieldTypes.ChunkString, order = 1)
    private String type;

    public ContactGroupModel setData(String data) {
        this.data = data;
        return this;
    }

    public ContactGroupModel setType(String type) {
        this.type = type;
        return this;
    }

    public String getData() {
        return data;
    }

    public String getType() {
        return type;
    }

}
