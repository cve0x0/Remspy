package com.remspy.modules.providers.contacts.models;

import com.remspy.net.annotations.TransferField;
import com.remspy.net.enums.TransferFieldTypes;
import com.remspy.scheme.IResolverRecord;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ContactModel extends RealmObject implements IResolverRecord {

    @PrimaryKey
    @TransferField(type = TransferFieldTypes.Int, order = 0)
    private int id;

    @TransferField(type = TransferFieldTypes.ChunkString, order = 1)
    private String name;

    @TransferField(type = TransferFieldTypes.ChunkString, order = 2)
    private String note;

    @TransferField(type = TransferFieldTypes.List, order = 3)
    private RealmList<ContactGroupModel> contactAddresses;

    @TransferField(type = TransferFieldTypes.List, order = 4)
    private RealmList<ContactGroupModel> contactPhones;

    @TransferField(type = TransferFieldTypes.List, order = 5)
    private RealmList<ContactGroupModel> contactEmails;

    @TransferField(type = TransferFieldTypes.List, order = 6)
    private RealmList<ContactGroupModel> contactIMs;

    @Override
    public int getId() {
        return id;
    }

    public ContactModel setId(int id) {
        this.id = id;
        return this;
    }

    public ContactModel setName(String name) {
        this.name = name;
        return this;
    }

    public String getName() {
        return name;
    }

    public ContactModel setNote(String note) {
        this.note = note;
        return this;
    }

    public String getNote() {
        return note;
    }

    public ContactModel setContactAddresses(RealmList<ContactGroupModel> contactAddresses) {
        this.contactAddresses = contactAddresses;
        return this;
    }

    public ContactModel setContactPhones(RealmList<ContactGroupModel> contactPhones) {
        this.contactPhones = contactPhones;
        return this;
    }

    public ContactModel setContactEmails(RealmList<ContactGroupModel> contactEmails) {
        this.contactEmails = contactEmails;
        return this;
    }

    public ContactModel setContactIMs(RealmList<ContactGroupModel> contactIMs) {
        this.contactIMs = contactIMs;
        return this;
    }

}
