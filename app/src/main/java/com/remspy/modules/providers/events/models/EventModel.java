package com.remspy.modules.providers.events.models;

import com.remspy.net.annotations.TransferField;
import com.remspy.net.enums.TransferFieldTypes;
import com.remspy.scheme.IResolverRecord;

import io.realm.RealmObject;

public class EventModel extends RealmObject implements IResolverRecord {

    @TransferField(type = TransferFieldTypes.Int, order = 0)
    private int id;

    @TransferField(type = TransferFieldTypes.Long, order = 1)
    private long startDate;

    @TransferField(type = TransferFieldTypes.Long, order = 2)
    private long endDate;

    @TransferField(type = TransferFieldTypes.ChunkString, order = 3)
    private String location;

    @TransferField(type = TransferFieldTypes.ChunkString, order = 4)
    private String description;

    @Override
    public int getId() {
        return id;
    }

    public EventModel setId(int id) {
        this.id = id;
        return this;
    }

    public EventModel setStartDate(long startDate) {
        this.startDate = startDate;
        return this;
    }

    public EventModel setEndDate(long endDate) {
        this.endDate = endDate;
        return this;
    }

    public EventModel setLocation(String location) {
        this.location = location;
        return this;
    }

    public EventModel setDescription(String description) {
        this.description = description;
        return this;
    }

}
