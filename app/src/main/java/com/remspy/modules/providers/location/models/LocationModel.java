package com.remspy.modules.providers.location.models;

import com.remspy.net.annotations.TransferField;
import com.remspy.net.enums.TransferFieldTypes;
import com.remspy.scheme.IResolverRecord;

import io.realm.RealmObject;

public class LocationModel extends RealmObject implements IResolverRecord {

    private int id;

    @TransferField(type = TransferFieldTypes.Double, order = 0)
    private double latitude;

    @TransferField(type = TransferFieldTypes.Double, order = 1)
    private double longitude;

    @TransferField(type = TransferFieldTypes.Long, order = 2)
    private long timestamp;

    @TransferField(type = TransferFieldTypes.Float, order = 3)
    private float accuracy;

    @TransferField(type = TransferFieldTypes.Double, order = 4)
    private double altitude;

    @Override
    public int getId() {
        return id;
    }

    public LocationModel setId(int id) {
        this.id = id;
        return this;
    }

    public LocationModel setLatitude(double latitude) {
        this.latitude = latitude;
        return this;
    }

    public LocationModel setLongitude(double longitude) {
        this.longitude = longitude;
        return this;
    }

    public LocationModel setTimestamp(long timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public LocationModel setAccuracy(float accuracy) {
        this.accuracy = accuracy;
        return this;
    }

    public LocationModel setAltitude(double altitude) {
        this.altitude = altitude;
        return this;
    }

}
