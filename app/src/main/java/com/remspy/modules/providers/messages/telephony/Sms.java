package com.remspy.modules.providers.messages.telephony;

import android.net.Uri;
import android.provider.BaseColumns;

public class Sms implements BaseColumns {

    public static final Uri CONTENT_URI = Uri.parse("content://sms");

    public static final String ADDRESS = "address";

    public static final String BODY = "body";

    public static final String TYPE = "type";

}