package com.remspy.modules.providers.photos;

import android.util.Log;

import com.remspy.annotations.Inject;
import com.remspy.common.ProviderTransfer;
import com.remspy.modules.holders.ContextHolder;
import com.remspy.modules.providers.photos.models.PhotoModel;
import com.remspy.modules.storage.StorageModule;
import com.remspy.modules.storage.models.BlockRecord;
import com.remspy.net.BufferStream;
import com.remspy.scheme.IResolverRecord;
import com.remspy.utils.ArrayUtils;

import java.io.File;

import io.realm.Realm;
import io.realm.RealmResults;

public class PhotosTransfer extends ProviderTransfer {

    @Inject
    private ContextHolder mContextHolder;

    @Inject
    private StorageModule mStorageModel;

    public PhotosTransfer(byte ins) {
        super(PhotoModel.class, ins);
    }

    @Override
    protected void putRecordToBuffer(BufferStream buffer, IResolverRecord record) throws Exception {
        PhotoModel photoModel = (PhotoModel) record;

        buffer.putInt(photoModel.getId());

        BlockRecord blockRecord = mStorageModel.getBlockRecord(photoModel.getBlockRecordId());
        if (blockRecord == null) {
            throw new Exception("Block record not found.");
        }

        File image = mContextHolder.getContext().getFileStreamPath(blockRecord.getFilename());

        buffer.putChunkString(blockRecord.getFilename());
        buffer.putFile(image);

        buffer.putLong(photoModel.getTimestamp());
        buffer.setBoolean(photoModel.isHaveLocations());

        if (photoModel.isHaveLocations()) {
            buffer.putDouble(photoModel.getLatitude());
            buffer.putDouble(photoModel.getLongitude());
        }
    }

    @Override
    protected void onRecordsConfirm(Realm realm, int[] confirmRecords) {
        RealmResults<PhotoModel> records = realm.where(PhotoModel.class)
                .in("id", ArrayUtils.asIntegerArray(confirmRecords))
                .findAll();

        for (PhotoModel photoModel: records) {
            mStorageModel.remove(photoModel.getBlockRecordId());
        }

        records.deleteAllFromRealm();
    }

}
