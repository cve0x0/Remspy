package com.remspy.net;

import com.remspy.net.interfaces.OnBufferReceive;
import com.remspy.net.utils.BufferUtils;

import java.nio.ByteBuffer;

public class PackageReader {

    protected ByteBuffer mPackageHeader;

    protected ByteBuffer mPackageBuffer;

    protected boolean mHavePackageId;

    protected int mPackageId;

    public PackageReader() {
        this(true);
    }

    public PackageReader(boolean havePackageId) {
        mHavePackageId = havePackageId;
        initHeaderBuffer();
    }

    protected ByteBuffer initHeaderBuffer(int size) {
        return BufferUtils.allocate(size);
    }

    protected void initHeaderBuffer() {
        int headerSize = 4;
        if (mHavePackageId) {
            headerSize += 4;
        }

        mPackageHeader = initHeaderBuffer(headerSize);
    }

    public boolean put(ByteBuffer buffer, OnBufferReceive cb) {
        while (buffer.hasRemaining()) {
            ByteBuffer streamBuffer = getStreamBuffer();

            int limit = buffer.limit();
            if (buffer.remaining() > streamBuffer.remaining()) {
                buffer.limit(buffer.position() + streamBuffer.remaining());
            }

            streamBuffer.put(buffer);
            buffer.limit(limit);

            if (isPackageReady()) {
                cb.onBufferReceive(getPackage());
                continue;
            }

            if (isHeaderReady() && !parseHeader()) {
                return false;
            }
        }

        return true;
    }

    private ByteBuffer getStreamBuffer() {
        return mPackageHeader.hasRemaining() ? mPackageHeader : mPackageBuffer;
    }

    private boolean isPackageReady() {
        return mPackageBuffer != null && !mPackageBuffer.hasRemaining();
    }

    private boolean isHeaderReady() {
        return !mPackageHeader.hasRemaining();
    }

    protected boolean onParseHeader() {
        return true;
    }

    protected boolean parseHeader() {
        mPackageHeader.flip();

        if (!onParseHeader()) {
            return false;
        }

        if (mHavePackageId) {
            mPackageId = mPackageHeader.getInt();
        }

        int size = mPackageHeader.getInt();
        mPackageBuffer = ByteBuffer.allocateDirect(size);

        return true;
    }

    public int getPackageId() {
        return mPackageId;
    }

    private ByteBuffer getPackage() {
        mPackageHeader.clear();
        ByteBuffer buffer = mPackageBuffer.duplicate();
        buffer.flip();
        mPackageBuffer = null;
        return buffer;
    }

    public void clear() {
        mPackageHeader.clear();

        if (mPackageBuffer != null) {
            BufferUtils.free(mPackageBuffer);
            mPackageBuffer = null;
        }
    }

    public void free() {
        BufferUtils.free(mPackageHeader);

        if (mPackageBuffer != null) {
            BufferUtils.free(mPackageBuffer);
        }
    }

}
