package com.remspy.net.annotations;

import com.remspy.net.enums.TransferFieldTypes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface TransferField {

    TransferFieldTypes type();
    int order();
    Class<?> listGenericType() default TransferField.class;

}
