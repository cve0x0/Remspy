package com.remspy.net.enums;

public enum TransferFieldTypes {
    Bool,
    Byte,
    Short,
    Int,
    Long,
    Float,
    Double,
    String,
    ChunkString,
    List
}
