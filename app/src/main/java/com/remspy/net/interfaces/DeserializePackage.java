package com.remspy.net.interfaces;

import com.remspy.net.BufferStream;

public interface DeserializePackage {

    void onDeserialize(BufferStream buffer) throws Exception;

}
