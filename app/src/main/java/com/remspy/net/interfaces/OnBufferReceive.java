package com.remspy.net.interfaces;

import java.nio.ByteBuffer;

public interface OnBufferReceive {

    void onBufferReceive(ByteBuffer buffer);

}
