package com.remspy.net.interfaces;

import com.remspy.net.BufferStream;

public interface SerializablePackage {

    void onSerialize(BufferStream buffer) throws Exception;

}
