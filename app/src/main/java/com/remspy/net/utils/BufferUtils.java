package com.remspy.net.utils;

import java.nio.ByteBuffer;

public class BufferUtils {

    public static void free(ByteBuffer buffer) {

    }

    public static ByteBuffer allocate(int size) {
        return ByteBuffer.allocateDirect(size);
    }

    public static int[] toArray(ByteBuffer buffer) {
        int[] arr = new int[buffer.remaining()];

        buffer.mark();
        for (int i = 0; i < buffer.remaining(); ++i) {
            arr[i] = buffer.get();
        }
        buffer.reset();

        return arr;
    }

}