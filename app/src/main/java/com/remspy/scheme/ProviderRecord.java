package com.remspy.scheme;

import io.realm.RealmObject;
import io.realm.annotations.Index;

public class ProviderRecord extends RealmObject {

    @Index()
    private int provider;

    @Index()
    private int record;

    private int version;

    public ProviderRecord setRecord(int id) {
        this.record = id;
        return this;
    }

    public int getRecord() {
        return this.record;
    }

    public ProviderRecord setProvider(int provider) {
        this.provider = provider;
        return this;
    }

    public int getProvider() {
        return provider;
    }

    public ProviderRecord setVersion(int version) {
        this.version = version;
        return this;
    }

    public int getVersion() {
        return version;
    }

}
