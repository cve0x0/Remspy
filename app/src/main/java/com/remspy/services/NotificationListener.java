package com.remspy.services;

import android.os.Build;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.support.annotation.RequiresApi;

import com.remspy.interfaces.Module;

import java.util.ArrayList;

@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
public class NotificationListener extends NotificationListenerService implements Module {

    public ArrayList<String> blacklist = new ArrayList<>();

    @Override
    public void onCreate() {
        super.onCreate();

        blacklist.add("Remspy is running in the background");
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onNotificationPosted(StatusBarNotification sbn){
        if (sbn == null || !sbn.getPackageName().equals("android")) {
            return;
        }

        String title = sbn.getNotification().extras.getString("android.title", null);
        if (title == null || !blacklist.contains(title)) {
            return;
        }

        snoozeNotification(sbn.getKey(), 10000000000000L);
    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn){

    }

}
