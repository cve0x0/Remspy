package com.remspy.utils;

import android.util.SparseArray;
import android.util.SparseIntArray;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class ArrayUtils {

    public static Integer[] asIntegerArray(int[] intArr) {
        Integer[] arr = new Integer[intArr.length];

        for (int i = 0; i < intArr.length; ++i) {
            arr[i] = intArr[i];
        }

        return arr;
    }

    public static int[] asIntArray(Set<Integer> arraySet) {
        int[] arr = new int[arraySet.size()];

        int p = 0;
        for (Integer i: arraySet) {
            arr[p++] = i;
        }

        return arr;
    }

    public static int[] asArray(SparseArray<int[]> sparseArray) {
        int size = 0;
        for (int i = 0; i < sparseArray.size(); ++i) {
            int key = sparseArray.keyAt(i);
            size += sparseArray.get(key).length;
        }

        int[] result = new int[size];
        int offset = 0;

        for (int i = 0; i < sparseArray.size(); ++i) {
            int key = sparseArray.keyAt(i);
            int[] value = sparseArray.get(key);
            System.arraycopy(value, 0, result, offset, value.length);
            offset += value.length;
        }

        return result;
    }

    public static Set<Integer> asIntSet(SparseIntArray sparseArray) {
        if (sparseArray == null) return null;
        Set<Integer> arrayList = new HashSet<>(sparseArray.size());
        for (int i = 0; i < sparseArray.size(); ++i)
            arrayList.add(sparseArray.keyAt(i));
        return arrayList;
    }

    public static String intArrToString(int[] arr) {
        String res = Arrays.toString(arr);
        return res.substring(1, res.length() - 1);
    }

    public static <T> boolean contains(final T[] array, final T v) {
        for (final T e : array)
            if (e == v || v != null && v.equals(e))
                return true;

        return false;
    }

    public static int[] resize(int[] array, int newSize) {
        if (array.length == newSize || newSize == 0) {
            return array;
        }

        int[] newArr = new int[newSize];
        System.arraycopy(array, 0, newArr, 0, (array.length > newSize ? newSize : array.length));
        return newArr;
    }

    public static int binarySearch(int[] array, int size, int value) {
        int lo = 0;
        int hi = size - 1;
        while (lo <= hi) {
            final int mid = (lo + hi) >>> 1;
            final int midVal = array[mid];
            if (midVal < value) {
                lo = mid + 1;
            } else if (midVal > value) {
                hi = mid - 1;
            } else {
                return mid;
            }
        }
        return ~lo;
    }

    public static int binarySearch(long[] array, int size, long value) {
        int lo = 0;
        int hi = size - 1;
        while (lo <= hi) {
            final int mid = (lo + hi) >>> 1;
            final long midVal = array[mid];
            if (midVal < value) {
                lo = mid + 1;
            } else if (midVal > value) {
                hi = mid - 1;
            } else {
                return mid;
            }
        }
        return ~lo;
    }

}
