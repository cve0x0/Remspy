package com.remspy.utils;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import io.realm.Realm;

public class Utils {

    public static ApplicationInfo getApplicationInfo(Context context) {
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return pInfo.applicationInfo;
        } catch (PackageManager.NameNotFoundException e) {
            return null;
        }
    }

    @SuppressWarnings("SameParameterValue")
    public static int realmAutoIncrement(Realm realm, Class object, String column) {
        try {
            Number lastRecord = realm.where(object).max(column);
            if (lastRecord == null) {
                return 1;
            }

            return lastRecord.intValue() + 1;
        } catch (Exception e) {
            return 1;
        }
    }

}
